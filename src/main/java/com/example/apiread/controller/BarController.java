package com.example.apiread.controller;

import com.example.apiread.model.Adress;
import com.example.apiread.model.Bar;
import com.example.apiread.repository.AdressRepository;
import com.example.apiread.repository.BarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/bar")
public class BarController {
    @Autowired
    BarRepository pr;

    @GetMapping("/")
    public ResponseEntity<List<Bar>> getAllProducts()
    {
        try {
            List<Bar> list = new ArrayList<Bar>(pr.findAll());

            if(list.isEmpty())
            {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(list, HttpStatus.OK);

        } catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Bar> getProductById(@PathVariable("id") String id)
    {
        Optional<Bar> productData = pr.findById(id);
        if(productData.isPresent())
        {
            return new ResponseEntity<>(productData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
