package com.example.apiread.controller;

import com.example.apiread.model.Photo;
import com.example.apiread.model.User;
import com.example.apiread.repository.PhotoRepository;
import com.example.apiread.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    UserRepository pr;

    @GetMapping("/")
    public ResponseEntity<List<User>> getAllProducts()
    {
        try {
            List<User> list = new ArrayList<User>(pr.findAll());

            if(list.isEmpty())
            {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(list, HttpStatus.OK);

        } catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getProductById(@PathVariable("id") String id)
    {
        Optional<User> productData = pr.findById(id);
        if(productData.isPresent())
        {
            return new ResponseEntity<>(productData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
