package com.example.apiread.controller;

import com.example.apiread.model.Product;
import com.example.apiread.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/product")
public class ProductController {
    @Autowired
    ProductRepository pr;

    @GetMapping("/")
    public ResponseEntity<List<Product>> getAllProducts()
    {
       try {
           List<Product> list = new ArrayList<Product>(pr.findAll());

           if(list.isEmpty())
           {
               return new ResponseEntity<>(HttpStatus.NO_CONTENT);
           }
        return new ResponseEntity<>(list, HttpStatus.OK);

       } catch (Exception e)
       {
           return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
       }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable("id") String id)
    {
        Optional<Product> productData = pr.findById(id);
        if(productData.isPresent())
        {
            return new ResponseEntity<>(productData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
