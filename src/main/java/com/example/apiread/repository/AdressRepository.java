package com.example.apiread.repository;

import com.example.apiread.model.Adress;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface AdressRepository extends MongoRepository<Adress, String> {
}
