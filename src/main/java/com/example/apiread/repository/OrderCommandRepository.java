package com.example.apiread.repository;

import com.example.apiread.model.OrderCommand;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface OrderCommandRepository extends MongoRepository<OrderCommand, String> {
}
