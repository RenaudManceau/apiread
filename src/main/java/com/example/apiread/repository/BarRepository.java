package com.example.apiread.repository;

import com.example.apiread.model.Bar;
import org.springframework.data.mongodb.repository.MongoRepository;
import java.util.List;

public interface BarRepository  extends MongoRepository<Bar, String> {
}
